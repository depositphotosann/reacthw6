import { useDispatch, useSelector } from "react-redux";
import styles from './Table.module.scss'
import Button from "../Button/Button";
import {ReactComponent as Star} from '../../img/13487794321580594410.svg'
import { addToFavorites, addToCart, openModal, removeFromFavorites} from '../../redux/actions';
import { useEffect, useState } from 'react';

function TableItem({info}) {

    const dispatch = useDispatch();
    const favorites = useSelector((state) => state.favorites)
    const productsInCart = useSelector((state) => state.cart )

    const addProductToFavorites = () => {
        dispatch(addToFavorites(info));
    }

    const removeProduct = () => {
        dispatch(removeFromFavorites(info));
    }

    function contains() {
        for (let i = 0; i < productsInCart.length; i++) {
            if (productsInCart[i].id === info.id ) {
                return true;
            }
        }
    }

    const addProductToCart = () => {
        if(!contains()){
            dispatch(addToCart(info))
        }
    }

    const showModal = () =>{
        dispatch(openModal({type: "Додати", payload: info}));
    }

    const [color, setColor] = useState()
    const {id:itemId} = info;

    useEffect(() =>{
        for (let i = 0; i <favorites.length; i++){
            if(favorites[i].id === itemId){
                setColor(true)
            }
        }
    } , [favorites, itemId])

    const changeColor = (color) =>{
        setColor(false)
    }

    function handler(){
        removeProduct();
        changeColor()
    }

    return (

    <tr>
        <td>{info.name}</td>
        <td><img className={styles.prod__img} src={info.url} alt="item" /></td>
        <td>{info.price} грн</td>
        <td><button className={styles.carg__to_favorite} ><Star className={color? styles.active: null} onClick={color? handler: addProductToFavorites}/></button></td>
        <td><Button backgroundColor = 'gray' text = 'Додати до кошика' onClick={() => {showModal()}} /></td>
    </tr>
     );
}

export default TableItem;