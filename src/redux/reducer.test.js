import {
  ADD_TO_CART,
  ADD_TO_FAVORITES,
  CLEAR_CART,
  CLOSE_MODAL,
  OPEN_MODAL,
  REMOVE_FROM_CART,
  REMOVE_FROM_FAVORITES,
  UPDATE_PRODUCT_LIST,
} from './actions';

import reducer from './reducer';

describe('reducer', () => {
  it('should handle UPDATE_PRODUCT_LIST', () => {
    const initialState = { products: [] };
    const updatedProducts = [{ id: 1, name: 'Product 1' }];
    const action = { type: UPDATE_PRODUCT_LIST, payload: updatedProducts };
    const newState = reducer(initialState, action);
    expect(newState.products).toEqual(updatedProducts);
  });

  it('should handle ADD_TO_FAVORITES', () => {
    const initialState = { favorites: [] };
    const productToAdd = { id: 1, name: 'Product 1' };
    const action = { type: ADD_TO_FAVORITES, payload: productToAdd };
    const newState = reducer(initialState, action);
    expect(newState.favorites).toContainEqual(productToAdd);
  });

  it('should handle REMOVE_FROM_FAVORITES', () => {
    const productToRemove = { id: 1, name: 'Product 1' };
    const initialState = { favorites: [productToRemove] };
    const action = { type: REMOVE_FROM_FAVORITES, payload: productToRemove };
    const newState = reducer(initialState, action);
    expect(newState.favorites).toEqual([]);
  });

  it('should handle ADD_TO_CART', () => {
    const initialState = { cart: [] };
    const productToAdd = { id: 1, name: 'Product 1' };
    const action = { type: ADD_TO_CART, payload: productToAdd };
    const newState = reducer(initialState, action);
    expect(newState.cart).toContainEqual(productToAdd);
  });

  it('should handle REMOVE_FROM_CART', () => {
    const productToRemove = { id: 1, name: 'Product 1' };
    const initialState = { cart: [productToRemove] };
    const action = { type: REMOVE_FROM_CART, payload: productToRemove };
    const newState = reducer(initialState, action);
    expect(newState.cart).toEqual([]);
  });

  it('should handle OPEN_MODAL', () => {
    const initialState = { modal: false };
    const action = { type: OPEN_MODAL, payload: true };
    const newState = reducer(initialState, action);
    expect(newState.modal).toBe(true);
  });

  it('should handle CLOSE_MODAL', () => {
    const initialState = { modal: true };
    const action = { type: CLOSE_MODAL, payload: false };
    const newState = reducer(initialState, action);
    expect(newState.modal).toBe(false);
  });

  it('should handle CLEAR_CART', () => {
    const initialState = { cart: [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }] };
    const action = { type: CLEAR_CART };
    const newState = reducer(initialState, action);
    expect(newState.cart).toEqual([]);
  });
});