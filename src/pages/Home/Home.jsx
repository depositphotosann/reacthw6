import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useContext } from 'react';
import { updateProductList } from '../../redux/actions';
import Card from '../../components/Card/card';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/Button/Button';
import Table from '../../components/Table/Table';
import ProductContext from '../../context';
import TableItem from '../../components/Table/TableItem';
import styles from './Home.module.scss'

function Home() {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(updateProductList());
  }, [dispatch]);

  const products = useSelector((state) => state.products);

  const {pageView, toggleView } = useContext(ProductContext)

  const productView = () => {
    if (pageView === 'Table'){
      return (
        <div className="products">
          {products.map(p => <Card key = {p.id} info={p} />)}
        </div>)

    } else if (pageView === 'Card'){
      return (
        <div className="products">
          <Table>
          {products.map(item => <TableItem key={item.id} info ={item}/>)}
          </Table>
      </div>
      )
    }
  }

    return (
      <>
        <div>
            <h1 className={styles.title}>Найкращі пропозиції від нашого магазину</h1>
            <Button backgroundColor = 'gray' text = {pageView} onClick={toggleView}/>
        </div>
        {productView()}
        <Modal text = 'Товар успішно додано в кошик!' />
          {/* <div className="products">
            {products.map(product => <Card key = {product.id} info={product} />)}
          </div> */}
      </>
     );
}

export default Home;