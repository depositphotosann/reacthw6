import { createContext, useState} from "react";

export const ProductContext = createContext();

export const ProductProvider = ({ children }) => {
    const [pageView, setPageView] = useState('Card');;

    const toggleView = () => {

        if (pageView === 'Card'){
          setPageView('Table')

        } else{
          setPageView('Card')
        }
      }
    return (
      <ProductContext.Provider value={{ pageView, toggleView }}>
        {children}
      </ProductContext.Provider>
    );
  };
  
export default ProductContext;